package repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import model.User;


public interface UserRepository extends CrudRepository<User, Long>{
	List<User> findAllByOrderByDateInscriptionAsc();
	List<User> findAllByOrderByDateInscriptionDesc();
	List<User> findAllByOrderByRaisonSocAsc();
	List<User> findAllByOrderByRaisonSocDesc();
	List<User> findBySiret(String siret);
	User findByEmail(String email);
}
