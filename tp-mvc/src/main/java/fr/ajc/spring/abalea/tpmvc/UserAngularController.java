package fr.ajc.spring.abalea.tpmvc;

import java.security.Principal;
import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import model.User;
import repo.UserRepository;

@RestController
@CrossOrigin
public class UserAngularController {
	
	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping(value="/login", method=RequestMethod.POST)
    public boolean login(@RequestBody User user) {
		
		User log = userRepository.findByEmail(user.getEmail());
		
        return
          user.getEmail().equals(log.getEmail()) && user.getMdp().equals(log.getMdp());
    }
     
    @RequestMapping(value="/user", method=RequestMethod.GET)
    public Principal user(HttpServletRequest request) {
        String authToken = request.getHeader("Authorization")
          .substring("Basic".length()).trim();
        return () ->  new String(Base64.getDecoder()
          .decode(authToken)).split(":")[0];
    }
}
