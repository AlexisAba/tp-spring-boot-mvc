package fr.ajc.spring.abalea.tpmvc;

import java.util.Locale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@EnableJpaRepositories("repo")
@EntityScan("model")
@SpringBootApplication
public class TpMvcApplication {

	public static void main(String[] args) {
		String encoded = new BCryptPasswordEncoder().encode("password");
		System.out.println(encoded);
		SpringApplication.run(TpMvcApplication.class, args);
	}
	
	

}
