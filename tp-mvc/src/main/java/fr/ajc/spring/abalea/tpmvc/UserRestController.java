package fr.ajc.spring.abalea.tpmvc;

import java.io.IOException;

import java.util.ArrayList;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import model.User;

import model.UserView;

import repo.UserRepository;



@RestController
@RequestMapping("/api/users")
public class UserRestController {
	
	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping(
			produces = { "application/json", "application/xml" },
			method = RequestMethod.GET)
	public @ResponseBody List<UserView> findAll(@RequestParam(value = "sort", required=false) String sort, @RequestParam(value = "order", required=false) String order) {
	List<User> liste;
	if(sort.equals("ByDate")) {
		System.out.println("ByDate");
		if(order.equals("Desc")) {
			liste = userRepository.findAllByOrderByDateInscriptionDesc();
		} else{
			liste = userRepository.findAllByOrderByDateInscriptionAsc();
		}
	} else if(sort.equals("ByName")) {
		System.out.println("ByName");
		if(order.equals("Desc")) {
			liste = userRepository.findAllByOrderByRaisonSocDesc();
		} else{
			liste = userRepository.findAllByOrderByRaisonSocAsc();
		}
	} else {
		System.out.println("passe");
		liste = (List) userRepository.findAll();
	}
	
		List<UserView> listeView = new ArrayList<UserView>();
			
		
		for (User user : liste) {
			listeView.add(new UserView(user.getRaisonSoc(), user.getTva(), user.getSiret(), user.getNbrSalarie(), user.isNewsletter()));
		}
		return listeView;
	}
	
	
	@RequestMapping(
			value = "/{siret}",
			produces = { "application/json", "application/xml" },
			method = RequestMethod.GET)
	public @ResponseBody ResponseEntity  search(@PathVariable String siret,HttpServletResponse httpResponse) throws IOException {
			
		List<User> list = userRepository.findBySiret(siret);
		
		if(list.isEmpty()) {
			httpResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return ResponseEntity
		            .status(HttpStatus.NOT_FOUND)
		            .body("Error Message");
		} else {
			httpResponse.setStatus(HttpServletResponse.SC_FOUND);
			return new ResponseEntity<List<User>>(list, HttpStatus.OK);
		}
	}

	
	@GetMapping("/delete/{id}")
	public void delete(@PathVariable Long id, HttpServletResponse httpResponse) throws IOException {
		userRepository.deleteById(id);
		httpResponse.sendRedirect("/delete");
	}
}
