package fr.ajc.spring.abalea.tpmvc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;


import model.User;
import repo.UserRepository;


@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	  protected void configure(AuthenticationManagerBuilder auth)
	  throws Exception {
	  PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
	  
	  List<User> users = (List<User>)userRepository.findAll();
	  
	  InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> mem = auth.inMemoryAuthentication();
	  
	  for(User user : users) {
		  mem.withUser(user.getEmail())
		  .password(encoder.encode(user.getMdp()))
		  .roles("USER");
	  }
	  
	  /*.withUser("api_consumer")
	  .password(encoder.encode("api_secret"))
	  .roles("USER")
	  .and()*/
	  mem.withUser("superman")
	  .password(encoder.encode("root"))
	  .roles("ADMIN");
	  }
	
	/*
	@Override
	protected void configure(HttpSecurity http) throws Exception {
	  http.csrf().disable()
	  .authorizeRequests()
	  .antMatchers("/connexion")
	  .authenticated()
	  .antMatchers("/")
	  .permitAll()
	  .and()
	  .formLogin()
	  .and()
	  .logout()
	  .logoutRequestMatcher(new AntPathRequestMatcher("/deconnexion"))
	  .logoutSuccessUrl("/")
	  .and();
	}
	*/
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
		.authorizeRequests()
		.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
		.antMatchers("/login").permitAll()
		.anyRequest()
		.authenticated()
		.and()
		.httpBasic();
	}
}
