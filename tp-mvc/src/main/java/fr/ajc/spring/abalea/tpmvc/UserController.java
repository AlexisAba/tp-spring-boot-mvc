package fr.ajc.spring.abalea.tpmvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import model.User;
import model.UserView;
import repo.UserRepository;

@Controller
public class UserController implements WebMvcConfigurer {
	
	
	
	@Autowired
	private UserRepository userRepository;
	
	@Value("${spring.application.name}")
	String appName;
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
    }
	
	@GetMapping("/")
	public String homePage(Model model) {
	
	model.addAttribute("appName", appName);
	return "home";
	}
	
	@PostMapping("/")
    public String checkPersonInfo(@Valid User user, BindingResult bindingResult, HttpServletResponse httpResponse) {
		user.setDateInscription(new Date());
		//Calcul tva
		String siret = user.getSiret();
		String siren = siret.substring(0, 9);
		int CleTVA = ( 12 + 3 * ( Integer.parseInt(siren) % 97 ) ) % 97;
		
		user.setTva("Fr"+CleTVA+siren);
		
        if (bindingResult.hasErrors()) {
        	httpResponse.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
        	
            return "inscription";
        } else {
        	httpResponse.setStatus(HttpServletResponse.SC_CREATED);
            userRepository.save(user);
            return "redirect:/inscription/success";
        }
        
    }
	
	@GetMapping("/inscription")
	public String inscription(Model model) {
	
	model.addAttribute("user", new User());
	model.addAttribute("appName", appName);
	
	return "inscription";
	}
	
	
	
	@GetMapping("/inscription/success")
	public String inscriptionSuccess(Model model) {
	
	model.addAttribute("appName", appName);
	
	return "success";
	}
	
	
	
	
	

	@GetMapping("/liste")
	public String liste(Model model) {

	List<User> liste = (List) userRepository.findAll();
		
	List<UserView> listeView = new ArrayList<UserView>();
		
	for (User user : liste) {
		listeView.add(new UserView(user.getRaisonSoc(), user.getTva(), user.getSiret(), user.getNbrSalarie(), user.isNewsletter()));
	}

	model.addAttribute("listUser", listeView);
	
	//Iterable<User> liste = userRepository.findAll();
	//model.addAttribute("listUser", liste);
	model.addAttribute("appName", appName);
	
	return "liste";
	}

	
}
