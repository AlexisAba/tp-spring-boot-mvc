package model;

import java.util.Date;


public class UserView {

	private String raisonSoc;
	private String tva;
	private String siret;
	private int nbrSalarie;
	private boolean newsletter;
	private static Date dateInscription;
	
	public UserView(String raisonSoc, String tva, String siret, int nbrSalarie, boolean newsletter) {
	
		this.raisonSoc = raisonSoc;
		this.tva = tva;
		this.siret = siret;
		this.nbrSalarie = nbrSalarie;
		this.newsletter = newsletter;
	}
	
	public UserView() {
		
	}
	
	
	public String getRaisonSoc() {
		return raisonSoc;
	}
	public void setRaisonSoc(String raisonSoc) {
		this.raisonSoc = raisonSoc;
	}
	public String getTva() {
		return tva;
	}
	public void setTva(String tva) {
		this.tva = tva;
	}
	public String getSiret() {
		return siret;
	}
	public void setSiret(String siret) {
		this.siret = siret;
	}
	public int getNbrSalarie() {
		return nbrSalarie;
	}
	public void setNbrSalarie(int nbrSalarie) {
		this.nbrSalarie = nbrSalarie;
	}
	public boolean isNewsletter() {
		return newsletter;
	}
	public void setNewsletter(boolean newsletter) {
		this.newsletter = newsletter;
	}
	public static Date getDateInscription() {
		return dateInscription;
	}
	public static void setDateInscription(Date dateInscription) {
		UserView.dateInscription = dateInscription;
	}

	@Override
	public String toString() {
		return "UserView [raisonSoc=" + raisonSoc + ", tva=" + tva + ", siret=" + siret + ", nbrSalarie=" + nbrSalarie
				+ ", newsletter=" + newsletter + "]";
	}
	
	
	
	
	
}
