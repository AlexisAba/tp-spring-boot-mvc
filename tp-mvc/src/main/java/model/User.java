package model;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.query.criteria.internal.expression.function.SubstringFunction;

@Entity
public class User {
	
	/*
	• Adresse email (obligatoire, expression régulière : RFC822)
	• Mot de passe (obligatoire, au moins 6 caractères dont un chiffre, une minuscule, une majuscule)
	• Raison social (obligatoire)
	• N°TVA Intracom. (facultatif mais qui doit être valide)
	• Numéro de SIRET (obligatoire et valide)
	• nombre de salariés (facultatif mais valide, au moins 5 salariés)
	• Inscription à la newsletter (boolean)
	• date d’inscription
	*/
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Email(message = "L\''email n\''est pas correct")
	@NotNull
	@Column(unique = true)
	private String email;
	
	@Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{6,}$", message="Le mot de passe doit contenir 6 caractère, 1 majuscule, 1 minuscule, 1 chiffre minimum")
	@NotNull
	@Column(unique = false)
	private String mdp;
	
	@NotNull
	@Column(unique = true)
	private String raisonSoc;
	
	@Column(unique = false)
	private String tva;
	
	@NotNull
	@Size(min=14, max=14)
	@Column(unique = true)
	private String siret;
	
	@Min(value=5)
	@Column(unique = false)
	private int nbrSalarie;
	
	@Column(unique = false)
	private boolean newsletter;
	
	@Column(unique = false)
	private Date dateInscription;
	
	public User(String email, String mdp, String raisonSoc,String tva, String siret, int nbrSalarie, Date dateInscription,
			boolean newsletter) {
		this.email = email;
		this.mdp = mdp;
		this.raisonSoc = raisonSoc;
		
		this.siret = siret;
		this.nbrSalarie = nbrSalarie;
		this.newsletter = newsletter;
		this.dateInscription = dateInscription;
		this.tva = tva;
		
	}
	
	public User() {
		
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	public String getRaisonSoc() {
		return raisonSoc;
	}
	public void setRaisonSoc(String raisonSoc) {
		this.raisonSoc = raisonSoc;
	}
	public String getTva() {
		return tva;
	}
	public void setTva(String tva) {
		this.tva = tva;
	}
	public String getSiret() {
		return siret;
	}
	public void setSiret(String siret) {
		this.siret = siret;
	}
	public int getNbrSalarie() {
		return nbrSalarie;
	}
	public void setNbrSalarie(int nbrSalarie) {
		this.nbrSalarie = nbrSalarie;
	}
	public boolean isNewsletter() {
		return newsletter;
	}
	public void setNewsletter(boolean newsletter) {
		this.newsletter = newsletter;
	}
	public Date getDateInscription() {
		return dateInscription;
	}
	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Entreprise [email=" + email + ", mdp=" + mdp + ", raisonSoc=" + raisonSoc + ", tva=" + tva + ", siret="
				+ siret + ", nbrSalarie=" + nbrSalarie + ", newsletter=" + newsletter + ", dateInscription="
				+ dateInscription + "]";
	}
	
	
}
